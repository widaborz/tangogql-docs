""" Test schema mutation """
import sys
from unittest.mock import MagicMock

# before importing code under test we need to import the mock database
# proxy we are going to use for stubbing
from tests.unit.test_schema_mocks import MockDB, MockDeviceProxyCache

mock_base = MagicMock()
sys.modules["tangogql.schema.base"] = mock_base
mock_base.db = MockDB()
mock_base.proxies = MockDeviceProxyCache()

import pytest

# now these will be imported with references to the mocks
# TODO the base should be refactored at some stage so we can insert mocks more easily
from tangogql.schema.mutations import (
    DeleteDeviceProperty,
    ExecuteDeviceCommand,
    PutDeviceProperty,
    SetAttributeValue,
    get_username_from_info,
)


@pytest.fixture
def info():
    """ Mock authentification infos """
    # mock = MagicMock()
    # mock.context = mock
    # mock.__getitem__.return_value = mock
    # mock.groups =  ["group"]
    # mock.required_groups = ["group"]
    # mock.user = "fred"
    fake_info = type("info", (), {})()
    config = type("config", (), {})()
    client = type("client", (), {})()
    config.required_groups = ["group"]
    client.user = "fred"
    client.groups = ["group"]
    fake_info.context = {"config": config, "client": client}

    yield fake_info


@pytest.fixture
def device_test_attributes(info):
    """ deviceData fixture used by unit tests to send queries to the TangoGQL code
    simulating requests from WebJive"""
    device_attributes = {
        "argin": "",
        "info": info,
        "command": "Status",
        "device": "sys / database / 2",
        "name": "RandomAttribute",
        "value": 20,
    }
    yield device_attributes


# common functionality used within mutation methods
def test_can_extract_name_from_info_block(device_test_attributes):
    """
    basic test of extracting the name from the information block provided - this is used
    inside all of the mutate functions as we only currently use the info to get the
    user name
    """
    name = "fred"
    assert name == get_username_from_info(device_test_attributes["info"])


# Test of mutation classes
# TODO: We should expand these tests and start organising them into classes

@pytest.mark.asyncio
async def test_basic_execute_device_command_mutate_works(device_test_attributes):
    result = await ExecuteDeviceCommand().mutate(
        device_test_attributes["info"],
        device_test_attributes["device"],
        device_test_attributes["command"],
        device_test_attributes["argin"],
    )
    assert result.message[0] == "Success"


@pytest.mark.asyncio
async def test_basic_set_device_attribute_value_mutate_works(device_test_attributes):
    result = await SetAttributeValue().mutate(
        device_test_attributes["info"],
        device_test_attributes["device"],
        device_test_attributes["name"],
        device_test_attributes["value"],
    )
    assert result.message[0] == "Success"


def test_basic_put_device_property_mutate_works(device_test_attributes):
    result = PutDeviceProperty().mutate(
        device_test_attributes["info"],
        device_test_attributes["device"],
        device_test_attributes["name"],
        device_test_attributes["value"],
    )
    assert result.message[0] == "Success"


def test_basic_delete_device_property_mutate_works(device_test_attributes):
    result = DeleteDeviceProperty().mutate(
        device_test_attributes["info"],
        device_test_attributes["device"],
        device_test_attributes["name"],
    )
    assert result.message[0] == "Success"
